package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"

	"gitlab.com/sciamp/fe-utils/internal/configuration"
	"gitlab.com/sciamp/fe-utils/pkg/component"
)

func main() {
	app := &cli.App{
		Name:  "feu",
		Usage: "Front end development utilities",
		Commands: []*cli.Command{
			{
				Name:  "component",
				Usage: "handle operations on components",
				Subcommands: []*cli.Command{
					{
						Name:      "new",
						Usage:     "Create component folder structure",
						ArgsUsage: "[ComponentName]",
						Action: func(c *cli.Context) error {
							conf, err := configuration.Read()

							if err != nil {
								panic(err)
							}

							return component.New(
								conf.Project.Code,
								conf.Component.Path,
								conf.Component.Type,
								c.Args().First(),
							).Run()
						},
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
