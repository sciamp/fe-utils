package configuration

import (
	"os"
	"path"
)

func Read() (Conf, error) {
	reader := Reader{
		localConfPath:  path.Join("./", ".feu.conf.yml"),
		globalConfPath: path.Join(os.Getenv("HOME"), ".fe-utils", "feu.conf.yml"),
		load:           load,
		parse:          parse,
	}

	return reader.read()
}
