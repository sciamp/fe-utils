package templates

import (
	"testing"
)

func TestLoadTemplate(t *testing.T) {
	templateName := "component.class.ts.template"
	template, err := Load(templateName)

	if err != nil {
		t.Errorf("Cannot find template %s", templateName)
		return
	}

	expected := `import React from "react";

interface {{.ComponentName}}Props {}

interface {{.ComponentName}}State {}

class {{.ComponentName}} extends React.Component<{{.ComponentName}}Props, {{.ComponentName}}State> {
  public render() {
    return "Hello {{.ComponentName}}";
  }
}

export { {{.ComponentName}} };
`

	if template != expected {
		t.Errorf("Expected \n%s\n as a return value, got \n%s\n instead", expected, template)
	}
}

func TestLoadUnknownTemplate(t *testing.T) {
	templateName := "this.does.not.exist.template"
	_, err := Load(templateName)

	if err == nil {
		t.Error("Expected Load to raise an error")
	}
}
