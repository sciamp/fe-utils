package configuration

import "io/ioutil"

type Loader func(path string) ([]byte, error)

func load(path string) ([]byte, error) {
	return ioutil.ReadFile(path)
}
