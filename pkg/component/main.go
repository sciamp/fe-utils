package component

import componentNew "gitlab.com/sciamp/fe-utils/internal/component/new"

func New(projectPath string, componentsDir string, componentType string, componentName string) *componentNew.ComponentCreator {
	return componentNew.NewComponentCreator(
		projectPath,
		componentsDir,
		componentType,
		componentName,
	)
}
