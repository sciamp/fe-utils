package configuration

import "testing"

func TestParseValidConf(t *testing.T) {
	conf := `
project:
  code: ./src
component:
  path: components
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
`
	_, err := parse([]byte(conf))

	if err != nil {
		t.Errorf("Valid configuration should return no errors")
	}
}

func TestParseNonValidYaml(t *testing.T) {
	conf := `
project
  code: ./src
component:
  path: components
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
`
	_, err := parse([]byte(conf))

	if err == nil {
		t.Errorf("Non-valid yaml should return an error")
	}
}
