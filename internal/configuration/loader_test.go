package configuration

import (
	"os"
	"testing"
)

func TestLoadFile(t *testing.T) {
	_, err := os.Create("./feu.conf.yml")

	if err != nil {
		t.Errorf("Test setup failed: %s", err)
	}

	_, err = load("./feu.conf.yml")

	if err != nil {
		t.Errorf("Loader failed with: %s", err)
	}

	err = os.Remove("./feu.conf.yml")

	if err != nil {
		t.Error("Couldn't remove test file")
	}
}

func TestLoadNonExistingFile(t *testing.T) {
	_, err := load("./feu.conf.yml")

	if err == nil {
		t.Error("load should fail when trying to load a non-existing file")
	}
}
