package configuration

import "gopkg.in/yaml.v2"

type Parser func(conf []byte) (Conf, error)

func parse(confContent []byte) (Conf, error) {
	var conf Conf

	err := yaml.Unmarshal(confContent, &conf)

	return conf, err
}
