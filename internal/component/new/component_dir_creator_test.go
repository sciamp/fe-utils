package componentNew

import (
	"os"
	"path/filepath"
	"testing"
)

func TestComponentDirectoryCreation(t *testing.T) {
	// set up
	cleanUp()

	// test
	createComponentDir("./src", "components", "MyComponent")
	expectedComponentDir := filepath.Join(componentsPath, "my-component")

	if _, err := os.Stat(expectedComponentDir); os.IsNotExist(err) {
		t.Errorf("Expected %s path to be created, command did not create any path.", expectedComponentDir)
	}

	// clean up
	cleanUp()
}

func TestComponentDirectoryReturnValue(t *testing.T) {
	// set up
	cleanUp()

	// test
	result, _ := createComponentDir("./src", "components", "MyComponent")
	expected := filepath.Join(componentsPath, "my-component")

	if result != expected {
		t.Errorf("Expected %s as a return value, got %s instead", expected, result)
	}

	// clean up
	cleanUp()
}
