package componentNew

import (
	"fmt"
	"strings"
	"testing"
)

func TestRunSequence(t *testing.T) {
	type CallRecord struct {
		functionName string
		parameters   []string
	}
	callSeq := make([]CallRecord, 0)
	expectedSeq := []CallRecord{
		CallRecord{
			functionName: "createComponentDir",
			parameters:   []string{"./src", "components", "MyComponent"},
		},
		CallRecord{
			functionName: "createComponentIndex",
			parameters:   []string{"./src/components/my-component/", "MyComponent"},
		},
		CallRecord{
			functionName: "createComponentFile",
			parameters:   []string{"./src/components/my-component/", "MyComponent", "func"},
		},
	}

	cc := buildComponentCreator(
		"./src",
		"components",
		"func",
		"MyComponent",
		func(projectPath string, componentsDir string, componentName string) (string, error) {
			callSeq = append(callSeq, CallRecord{
				functionName: "createComponentDir",
				parameters:   []string{projectPath, componentsDir, componentName},
			})
			return "./src/components/my-component/", nil
		},
		func(componentDir string, componentName string) (string, error) {
			callSeq = append(callSeq, CallRecord{
				functionName: "createComponentIndex",
				parameters:   []string{componentDir, componentName},
			})
			return "./src/components/my-component/index.ts", nil
		},
		func(componentDir string, componentName string, componentType string) (string, error) {
			callSeq = append(callSeq, CallRecord{
				functionName: "createComponentFile",
				parameters:   []string{componentDir, componentName, componentType},
			})
			return "./src/components/my-component/my-component.tsx", nil
		},
	)

	cc.Run()

	if len(callSeq) != len(expectedSeq) {
		t.Errorf("Expected %d function calls, found: %d", len(expectedSeq), len(callSeq))
	}

	for i, callRecord := range callSeq {
		expectedSeqString := fmt.Sprintf("%s(%s)",
			expectedSeq[i].functionName,
			strings.Join(expectedSeq[i].parameters, ", "),
		)
		callRecordString := fmt.Sprintf("%s(%s)",
			callRecord.functionName,
			strings.Join(callRecord.parameters, ", "),
		)
		if callRecordString != expectedSeqString {
			t.Errorf("Expecting call #%d, to be\n%s\ngot\n%s\ninstead", i, expectedSeqString, callRecordString)
		}
	}

}
