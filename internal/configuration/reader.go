package configuration

import (
	"fmt"

	"github.com/imdario/mergo"
)

type Project struct {
	Code string
}

type Component struct {
	Path string
	Type string
}

type Conf struct {
	Project   *Project
	Component *Component
}

type Reader struct {
	localConfPath  string
	globalConfPath string
	load           Loader
	parse          Parser
}

func (c Reader) read() (Conf, error) {
	localConfFile, localError := c.load(c.localConfPath)

	globalConfFile, globalError := c.load(c.globalConfPath)

	if localError != nil && globalError != nil {
		return Conf{}, fmt.Errorf("At least one of local or global conf file should be present")
	}

	localConf, _ := c.parse(localConfFile)
	globalConf, _ := c.parse(globalConfFile)

	mergeError := mergo.Merge(&localConf, globalConf)

	return localConf, mergeError
}
