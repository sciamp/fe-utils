package componentNew

func NewComponentCreator(
	projectPath string,
	componentsDir string,
	componentType string,
	componentName string,
) *ComponentCreator {
	return buildComponentCreator(
		projectPath,
		componentsDir,
		componentType,
		componentName,
		createComponentDir,
		createComponentIndex,
		createComponentFile,
	)
}
