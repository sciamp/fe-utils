This project is a collection of tool to speed up front end development.

Developing every day React + Typescript code I started dreaming of something to avoid typing the same commands over and over :)

## Installation

0. Verify that you have Go 1.13+ installed
```
$ go version
go version go1.13.7
```

1. Clone the repo
```
$ git clone https://gitlab.com/sciamp/fe-utils.git ~/.fe-utils
```

2. Compile
```
$ cd ~/.fe-utils && make install
```

3. Add `~/.fe-utils/bin` to your $PATH

  * For **bash**:
  ~~~ bash
  $ echo 'export PATH="$HOME/.fe-utils/bin:$PATH"' >> ~/.bash_profile
  ~~~

  * For **Zsh**:
  ~~~ zsh
  $ echo 'export PATH="$HOME/.fe-utils/bin:$PATH"' >> ~/.zshrc
  ~~~

  * For **Fish shell**:
  ~~~ fish
  $ set -Ux fish_user_paths $HOME/.fe-utils/bin $fish_user_paths
  ~~~

4. Restart your shell so that PATH changes take effect.

## Configuration

There are several configuration you can set project wise to reduce stuff you have to type when using `feu`. Configurations take place in a `.feu.conf.yml` file inside your project (if not found fallbacks to `~/.fe-utils/feu.conf.yml`):

```yaml
---
project:
  code: src
components:
  path: components
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
```

## New component

The `component new` command will create a new (empty) component:

```
$ cd /my/project
$ feu component new MyComponent
```

This will result in:

```
/my/project/
 + src/
   + components/
     + my-component/
       | index.ts
       | my-component.tsx
```

```ts
// src/components/my-component/index.ts
export { MyComponent } from "./my-component";
```

```ts
// src/components/my-component/my-component.tsx
import React from "react";

interface MyComponentProps {}

const MyComponent: React.FC<MyComponentProps> = ({}) => "Hello MyComponent";

export { MyComponent };
```

While in case we asked for class component:

```ts
// src/components/my-component/my-component.tsx
import React from "react";

interface MyComponentProps {}

interface MyComponentState {}

class MyComponent extends React.Component<MyComponentProps, MyComponentState> {
  public render() {
    return "Hello MyComponent";
  }
}

export { MyComponent };
```
