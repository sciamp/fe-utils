package componentNew

type ComponentCreator struct {
	createComponentDir   ComponentDirCreator
	createComponentIndex ComponentIndexCreator
	createComponentFile  ComponentFileCreator
	projectPath          string
	componentsDir        string
	componentType        string
	componentName        string
}

func (c ComponentCreator) Run() error {
	componentDir, err := c.createComponentDir(c.projectPath, c.componentsDir, c.componentName)

	if err != nil {
		return err
	}
	_, err = c.createComponentIndex(componentDir, c.componentName)

	if err != nil {
		return err
	}

	_, err = c.createComponentFile(componentDir, c.componentName, c.componentType)

	if err != nil {
		return err
	}

	return nil
}

func buildComponentCreator(
	projectPath string,
	componentsDir string,
	componentType string,
	componentName string,
	createComponentDir ComponentDirCreator,
	createComponentIndex ComponentIndexCreator,
	createComponentFile ComponentFileCreator,
) *ComponentCreator {
	return &ComponentCreator{
		projectPath:          projectPath,
		componentsDir:        componentsDir,
		componentType:        componentType,
		componentName:        componentName,
		createComponentDir:   createComponentDir,
		createComponentIndex: createComponentIndex,
		createComponentFile:  createComponentFile,
	}
}
