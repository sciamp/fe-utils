package componentNew

import (
	"os"
	"path/filepath"
	"text/template"

	"github.com/iancoleman/strcase"
	"gitlab.com/sciamp/fe-utils/internal/templates"
)

type ComponentIndexCreator func(componentPath string, componentName string) (string, error)

func createComponentIndex(componentPath string, componentName string) (string, error) {
	indexPath := filepath.Join(componentPath, "index.ts")
	destFile, err := os.Create(indexPath)

	if err != nil {
		return "", err
	}

	indexTemplate, err := templates.Load("index.ts.template")

	if err != nil {
		return "", err
	}

	tmpl, err := template.New("index.ts").Parse(string(indexTemplate))

	if err != nil {
		return "", err
	}

	type Index struct {
		ComponentName string
		ComponentFile string
	}

	err = tmpl.Execute(destFile, Index{componentName, strcase.ToKebab(componentName)})

	if err != nil {
		return "", err
	}

	return indexPath, nil
}
