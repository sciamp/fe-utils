package componentNew

import (
	"os"
	"path/filepath"
)

var sourcePath = filepath.Join("./", "./src")
var componentsPath = filepath.Join(sourcePath, "components")

func cleanUp() {
	os.RemoveAll(sourcePath)
}
