package componentNew

import (
	"fmt"
	"os"
	"path/filepath"
	"text/template"

	"github.com/iancoleman/strcase"
	"gitlab.com/sciamp/fe-utils/internal/templates"
)

type ComponentFileCreator func(componentPath string, componentName string, componentType string) (string, error)

func createComponentFile(componentPath string, componentName string, componentType string) (string, error) {
	fileName := fmt.Sprintf("%s.tsx", strcase.ToKebab(componentName))
	componentFilePath := filepath.Join(componentPath, fileName)
	_, err := os.Create(componentFilePath)

	if err != nil {
		return "", err
	}

	destFile, err := os.Create(componentFilePath)

	if err != nil {
		return "", err
	}

	templateName := fmt.Sprintf("component.%s.ts.template", componentType)
	componentTemplate, err := templates.Load(templateName)

	if err != nil {
		return "", err
	}

	tmpl, err := template.New(fileName).Parse(string(componentTemplate))

	if err != nil {
		return "", err
	}

	type Component struct {
		ComponentName string
	}

	err = tmpl.Execute(destFile, Component{componentName})
	if err != nil {
		return "", err
	}

	return componentFilePath, nil
}
