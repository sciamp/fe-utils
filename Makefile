BINDIR		:= build
TARGETS     := darwin/amd64 linux/amd64 linux/386 linux/arm linux/arm64 linux/ppc64le linux/s390x windows/amd64
TARGET_OBJS ?= darwin-amd64.tar.gz darwin-amd64.tar.gz.sha256 linux-amd64.tar.gz linux-amd64.tar.gz.sha256 linux-386.tar.gz linux-386.tar.gz.sha256 linux-arm.tar.gz linux-arm.tar.gz.sha256 linux-arm64.tar.gz linux-arm64.tar.gz.sha256 linux-ppc64le.tar.gz linux-ppc64le.tar.gz.sha256 linux-s390x.tar.gz linux-s390x.tar.gz.sha256 windows-amd64.zip windows-amd64.zip.sha256
BINNAME     ?= feu

GOPATH		= $(shell go env GOPATH)
ARCH 		= $(shell uname -r)

PKG			:= ./...
TAGS 		:=
TESTS		:= .
TESTFLAGS	:= -v
LDFLAGS		:= -w -s
GOFLAGS		:=
SRC			:= $(shell find . -type f -name '*.go' -print)

shell		= /bin/bash

GIT_COMMIT = $(shell git rev-parse HEAD)
GIT_SHA    = $(shell git rev-parse --short HEAD)
GIT_TAG    = $(shell git describe --tags --abbrev=0 --exact-match 2>/dev/null)
GIT_DIRTY  = $(shell test -n "`git status --porcelain`" && echo "dirty" || echo "clean")

INSTALL_DIR	 = $(HOME)/.fe-utils
SAMPLE_CONF	 = ./data/feu.conf.yml.sample
INSTALL_CONF = $(INSTALL_DIR)/feu.conf.yml
INSTALL_BIN  = $(INSTALL_DIR)/bin

ifdef VERSION
	BINARY_VERSION = $(VERSION)
endif
BINARY_VERSION ?= ${GIT_TAG}

VERSION_METADATA = unreleased
# Clear the "unreleased" string in BuildMetadata
ifneq ($(GIT_TAG),)
	VERSION_METADATA =
endif

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: all
all: build

.PHONY: build
build: clean $(BINDIR)/$(BINNAME) ## Build binary

$(BINDIR)/$(BINNAME): $(SRC)
	GO111MODULE=on go build $(GOFLAGS) -tags '$(TAGS)' -ldflags '$(LDFLAGS)' -o $(BINDIR)/$(BINNAME) ./cmd/feu

.PHONY: clean
clean:
	@rm -rf $(BINDIR)

.PHONY: install
install: build ## Install feu binary to ~/.fe-utils
	@mkdir -p $(INSTALL_DIR)
	@cp $(SAMPLE_CONF) $(INSTALL_CONF)
	@cp -r $(BINDIR) $(INSTALL_BIN)

.PHONY: test
test: ## Run tests
	GO111MODULE=on go test $(GOFLAGS) -run $(TESTS) $(PKG) $(TESTFLAGS)
