package componentNew

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func TestIndexFileCreation(t *testing.T) {
	// set up
	cleanUp()
	componentPath, _ := createComponentDir("./src", "components", "MyComponent")

	// test
	createComponentIndex(componentPath, "MyComponent")
	expectedIndexFile := filepath.Join(componentPath, "index.ts")

	if _, err := os.Stat(expectedIndexFile); os.IsNotExist(err) {
		t.Errorf("Expected %s to be created, command did not create any file.", expectedIndexFile)
	}

	// clean up
	cleanUp()
}

func TestIndexFileCreationReturnValue(t *testing.T) {
	// set up
	cleanUp()
	componentPath, _ := createComponentDir("./src", "components", "MyComponent")

	// test
	result, _ := createComponentIndex(componentPath, "MyComponent")
	expected := filepath.Join(componentPath, "index.ts")

	if result != expected {
		t.Errorf("Expected %s as a return value, got %s instead", expected, result)
	}

	// clean up
	cleanUp()
}

func TestIndexFileContent(t *testing.T) {
	// set up
	cleanUp()
	componentPath, _ := createComponentDir("./src", "components", "MyComponent")

	// test
	result, _ := createComponentIndex(componentPath, "MyComponent")
	indexContent, err := ioutil.ReadFile(result)

	if err != nil {
		t.Errorf("Could not read file content: %s", err)
	}

	expected := `export { MyComponent } from "./my-component";
`

	if string(indexContent) != expected {
		t.Errorf("Expected content for index file is:\n%s\nGot\n%s\ninstead", expected, string(indexContent))
	}

	// clean up
	cleanUp()
}
