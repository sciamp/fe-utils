module gitlab.com/sciamp/fe-utils

go 1.13

require (
	github.com/goccy/go-yaml v1.1.4 // indirect
	github.com/iancoleman/strcase v0.0.0-20191112232945-16388991a334
	github.com/imdario/mergo v0.3.8
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/urfave/cli/v2 v2.1.1
	golang.org/x/sys v0.0.0-20191210023423-ac6580df4449 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
	gopkg.in/yaml.v2 v2.2.7
)
