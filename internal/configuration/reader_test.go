package configuration

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"testing"
)

var expected Conf = Conf{
	Project: &Project{
		Code: "./src",
	},
	Component: &Component{
		Path: "components",
		Type: "funct",
	},
}

func TestReadProjectCode(t *testing.T) {
	reader := Reader{
		localConfPath:  path.Join("./", ".feu.conf.yml"),
		globalConfPath: path.Join(os.Getenv("HOME"), ".feu.conf.yml"),
		load: func(_path string) ([]byte, error) {
			content := `
project:
  code: ./src
component:
  path: components
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
`
			return []byte(content), nil
		},
		parse: parse,
	}

	conf, err := reader.read()

	if err != nil {
		t.Error(err)
	}

	if conf.Project.Code != expected.Project.Code {
		t.Errorf("Conf value for \"project\" -> \"code\" was incorrect, got: %s instead of %s", conf.Project.Code, expected.Project.Code)
	}
}

func TestComponentPath(t *testing.T) {
	reader := Reader{
		localConfPath:  path.Join("./", ".feu.conf.yml"),
		globalConfPath: path.Join(os.Getenv("HOME"), ".feu.conf.yml"),
		load: func(_path string) ([]byte, error) {
			content := `
project:
  code: ./src
component:
  path: components
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
`
			return []byte(content), nil
		},
		parse: parse,
	}

	conf, err := reader.read()

	if err != nil {
		t.Error(err)
	}

	if conf.Component.Path != expected.Component.Path {
		t.Errorf("Conf value for \"component\" -> \"path\" was incorrect, got: %s instead of %s", conf.Component.Path, expected.Component.Path)
	}
}

func TestComponentType(t *testing.T) {
	reader := Reader{
		localConfPath:  path.Join("./", ".feu.conf.yml"),
		globalConfPath: path.Join(os.Getenv("HOME"), ".feu.conf.yml"),
		load: func(_path string) ([]byte, error) {
			content := `
project:
  code: ./src
component:
  path: components
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
`
			return []byte(content), nil
		},
		parse: parse,
	}

	conf, err := reader.read()

	if err != nil {
		t.Error(err)
	}

	if conf.Component.Type != expected.Component.Type {
		t.Errorf("Conf value for \"component\" -> \"type\" was incorrect, got: %s instead of %s", conf.Component.Type, expected.Component.Type)
	}
}

func TestLoadsLocalAndGlobalConf(t *testing.T) {
	type CallRecord struct {
		functionName string
		parameters   string
	}
	callSeq := make([]CallRecord, 0)
	expectedSeq := []CallRecord{
		CallRecord{
			functionName: "load",
			parameters:   path.Join("./", ".feu.conf.yml"),
		},
		CallRecord{
			functionName: "load",
			parameters:   path.Join(os.Getenv("HOME"), ".feu.conf.yml"),
		},
	}

	localConf := `
component:
  path: customComponentPath
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
`

	globalConf := `
project:
  code: ./src
component:
  path: components
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
`

	reader := Reader{
		localConfPath:  path.Join("./", ".feu.conf.yml"),
		globalConfPath: path.Join(os.Getenv("HOME"), ".feu.conf.yml"),
		load: func(loadPath string) ([]byte, error) {
			callSeq = append(callSeq, CallRecord{
				functionName: "load",
				parameters:   loadPath,
			})

			if loadPath == path.Join("./", ".feu.conf.yml") {
				return []byte(localConf), nil
			}

			if loadPath == path.Join(os.Getenv("HOME"), ".feu.conf.yml") {
				return []byte(globalConf), nil
			}

			return nil, fmt.Errorf("Cannot load requested path.")
		},
		parse: parse,
	}

	conf, _ := reader.read()

	if len(callSeq) != len(expectedSeq) {
		t.Errorf("Expected %d function calls, got %d", len(callSeq), len(expectedSeq))
	}

	for i, callRecord := range callSeq {
		expectedSeqString := fmt.Sprintf("%s(%s)",
			expectedSeq[i].functionName,
			expectedSeq[i].parameters,
		)
		callRecordString := fmt.Sprintf("%s(%s)",
			callRecord.functionName,
			callRecord.parameters,
		)
		if callRecordString != expectedSeqString {
			t.Errorf("Expecting call #%d, to be\n%s\ngot\n%s\ninstead", i, expectedSeqString, callRecordString)
		}
	}

	if conf.Project.Code != "./src" {
		t.Errorf("Expected project > code to fallback to global \"./src\", got \"%s\" instead.", conf.Project.Code)
	}

	if conf.Component.Path != "customComponentPath" {
		t.Errorf("Expected component > path to use the local \"customComponentPath\", got \"%s\" instead", conf.Component.Path)
	}
}

func TestLocalConfFileNotPresent(t *testing.T) {
	globalConf := `
project:
  code: ./src
component:
  path: components
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
`

	reader := Reader{
		localConfPath:  path.Join("./", ".feu.conf.yml"),
		globalConfPath: path.Join(os.Getenv("HOME"), ".feu.conf.yml"),
		load: func(loadPath string) ([]byte, error) {
			if loadPath == path.Join("./", ".feu.conf.yml") {
				return ioutil.ReadFile("this_file_does_not_exist.yml")
			}

			if loadPath == path.Join(os.Getenv("HOME"), ".feu.conf.yml") {
				return []byte(globalConf), nil
			}

			return nil, fmt.Errorf("Cannot load requested path.")
		},
		parse: parse,
	}

	_, err := reader.read()

	if err != nil {
		t.Errorf("Expected read() to complete without errors, got \"%s\" instead", err)
	}
}

func TestGlobalConfFileNotPresent(t *testing.T) {
	localConf := `
project:
  code: ./src
component:
  path: components
  type: funct # this will create a React.FC
  # type: class # this will create a class extending React.Component
`

	reader := Reader{
		localConfPath:  path.Join("./", ".feu.conf.yml"),
		globalConfPath: path.Join(os.Getenv("HOME"), ".feu.conf.yml"),
		load: func(loadPath string) ([]byte, error) {
			if loadPath == path.Join("./", ".feu.conf.yml") {
				return []byte(localConf), nil
			}

			if loadPath == path.Join(os.Getenv("HOME"), ".feu.conf.yml") {
				return ioutil.ReadFile("this_file_does_not_exist.yml")
			}

			return nil, fmt.Errorf("Cannot load requested path.")
		},
		parse: parse,
	}

	_, err := reader.read()

	if err != nil {
		t.Errorf("Expected read() to complete without errors, got \"%s\" instead", err)
	}
}

func TestBothConfFilesNotPresent(t *testing.T) {
	reader := Reader{
		localConfPath:  path.Join("./", ".feu.conf.yml"),
		globalConfPath: path.Join(os.Getenv("HOME"), ".feu.conf.yml"),
		load: func(loadPath string) ([]byte, error) {
			if loadPath == path.Join("./", ".feu.conf.yml") {
				return ioutil.ReadFile("this_file_does_not_exist.yml")
			}

			if loadPath == path.Join(os.Getenv("HOME"), ".feu.conf.yml") {
				return ioutil.ReadFile("this_file_too_does_not_exist.yml")
			}

			return nil, fmt.Errorf("Cannot load requested path.")
		},
		parse: parse,
	}

	expectedErrorMessage := "At least one of local or global conf file should be present"
	_, err := reader.read()

	if err.Error() != expectedErrorMessage {
		t.Errorf("Expected error message \"%s\", got \"%s\" instead", expectedErrorMessage, err)
	}
}
