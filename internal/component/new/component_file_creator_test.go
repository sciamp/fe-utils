package componentNew

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func TestComponentFileCreation(t *testing.T) {
	// set up
	cleanUp()
	componentPath, _ := createComponentDir("./src", "components", "MyComponent")

	// test
	createComponentFile(componentPath, "MyComponent", "funct")
	expectedComponentFile := filepath.Join(componentPath, "my-component.tsx")

	if _, err := os.Stat(expectedComponentFile); os.IsNotExist(err) {
		t.Errorf("Expected %s to be created, command did not create any file.", expectedComponentFile)
	}

	// clean up
	cleanUp()
}

func TestComponentFileContentForFunctType(t *testing.T) {
	// set up
	cleanUp()
	componentPath, _ := createComponentDir("./src", "components", "MyComponent")

	// test
	result, _ := createComponentFile(componentPath, "MyComponent", "funct")
	componentContent, err := ioutil.ReadFile(result)

	if err != nil {
		t.Errorf("Could not read file content: %s", err)
	}

	expected := `import React from "react";

interface MyComponentProps {}

const MyComponent: React.FC<MyComponentProps> = ({}) => "Hello MyComponent";

export { MyComponent };
`

	if string(componentContent) != expected {
		t.Errorf("Expected content for index file is:\n%s\nGot\n%s\ninstead", expected, string(componentContent))
	}

	// clean up
	cleanUp()
}

func TestComponentFileContentForClassType(t *testing.T) {
	// set up
	cleanUp()
	componentPath, _ := createComponentDir("./src", "components", "MyComponent")

	// test
	result, _ := createComponentFile(componentPath, "MyComponent", "class")
	componentContent, err := ioutil.ReadFile(result)

	if err != nil {
		t.Errorf("Could not read file content: %s", err)
	}

	expected := `import React from "react";

interface MyComponentProps {}

interface MyComponentState {}

class MyComponent extends React.Component<MyComponentProps, MyComponentState> {
  public render() {
    return "Hello MyComponent";
  }
}

export { MyComponent };
`

	if string(componentContent) != expected {
		t.Errorf("Expected content for index file is:\n%s\nGot\n%s\ninstead", expected, string(componentContent))
	}

	// clean up
	cleanUp()
}
