package componentNew

import (
	"os"
	"path/filepath"

	"github.com/iancoleman/strcase"
)

type ComponentDirCreator func(projectPath string, componentsDir string, componentName string) (string, error)

func createComponentDir(projectPath, componentsDir, componentName string) (string, error) {
	componentsPath := filepath.Join("./", projectPath, componentsDir)
	dirName := strcase.ToKebab(componentName)
	dirPath := filepath.Join(componentsPath, dirName)
	err := os.MkdirAll(dirPath, 0755)

	if err != nil {
		return "", err
	}

	return dirPath, nil
}
