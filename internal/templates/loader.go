package templates

import (
	"fmt"
)

const index = `export { {{.ComponentName}} } from "./{{.ComponentFile}}";
`

const componentClass = `import React from "react";

interface {{.ComponentName}}Props {}

interface {{.ComponentName}}State {}

class {{.ComponentName}} extends React.Component<{{.ComponentName}}Props, {{.ComponentName}}State> {
  public render() {
    return "Hello {{.ComponentName}}";
  }
}

export { {{.ComponentName}} };
`

const componentFunct = `import React from "react";

interface {{.ComponentName}}Props {}

const {{.ComponentName}}: React.FC<{{.ComponentName}}Props> = ({}) => "Hello {{.ComponentName}}";

export { {{.ComponentName}} };
`

var templates = map[string]string{
	"index.ts.template":           index,
	"component.class.ts.template": componentClass,
	"component.funct.ts.template": componentFunct,
}

func Load(templateName string) (string, error) {
	template, ok := templates[templateName]

	if !ok {
		return "", fmt.Errorf("Cannot find template %s.", templateName)
	}

	return template, nil
}
